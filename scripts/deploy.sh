#!/bin/sh
echo "Starting Netlify Deploy"
netlify deploy -s moonsquads -p _site -t $NETLIFY_TOKEN

echo "Starting Github Deploy"
# "Removing current git references"
rm -rf .git/
# "Cloning Repo"
git clone "https://isaacrg:$GH_PASS@github.com/moonsquads/moonsquads.github.io.git"
cd moonsquads.github.io
git rm * -r
ls
cp -r ../_site/* .
git add *
git config --global user.email "$GH_EMAIL"
git config --global user.name "$GH_NAME"
git commit -m "Updated files - via moonsquads/moonsquadstatic@$TRAVIS_COMMIT"
git config --global push.default simple
git push > ../log.txt 2>&1

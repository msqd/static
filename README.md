# moonsquads-static

[![Build Status](https://travis-ci.org/moonsquads/moonsquadstatic.svg?branch=master)](https://travis-ci.org/moonsquads/moonsquadstatic)

Hosted on Netlify at [moonsquads.netlify.com](https://moonsquads.netlify.com/), with the Admin panel at [moonsquads.netlify.com/admin/](https://moonsquads.netlify.com/admin/).

Guides are now at the [Github Wiki](https://github.com/moonsquads/moonsquads-static/wiki), and project is managed on [Trello](https://trello.com/moonsquads1)

## Todo

* [ ] Sort out the /admin/(netlify-cms) config, including for data folder.
